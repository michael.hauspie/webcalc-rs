use rocket::{get, http::uri::Host, launch, post, routes, serde::json::Json};

const INDEX_TEXT: &str = r#"\
Cette application permet d'executer des calculs simple. 

Pour l'utiliser, il faut envoyer le calcul à effectuer sous la forme d'un objet json dans une requete POST sur '/'

Le format à utiliser est un objet json qui a un attribu étant le nom
de l'operation à effectuer, dont la valeur est un tableau d'opérande
de taille 2

Exemple:

{
   "Plus": [3, 4]
}

L'application retourne un résultat au format json avec le résultat (nombre) et l'opération effectuée (chaîne)

Exemple:

{
   "operation": "3 + 4",
   "resultat": 7,
}

Les opérations possibles sont:

- "Plus"
- "Moins"
- "Fois"
- "Diviser"

Pour envoyer une requête avec curl:

curl -X POST http://HOST_URI -H 'Content-Type: application/json' -d '{"Plus": [3, 4]}'
"#;

mod operations;
use operations::{Operation, OperationResult};

#[get("/")]
fn index(host: &Host) -> String {
    INDEX_TEXT.replace("HOST_URI", &host.to_string())
}

#[post("/", data = "<operation>")]
fn do_calc(operation: Json<Operation>) -> Json<OperationResult> {
    OperationResult::from(operation.into_inner()).into()
}

#[get("/test")]
fn test() -> Json<Operation> {
    let t = Operation::Plus(1, 3);
    t.into()
}

#[launch]
fn launch() -> _ {
    let figment = rocket::Config::figment().merge(("address", "0.0.0.0"));

    rocket::custom(figment).mount("/", routes![index, do_calc, test])
}

#[cfg(test)]
mod tests {
    use super::*;
    use rocket::http::Status;
    use rocket::http::uri::Host;
    use rocket::local::blocking::Client;
    use rocket::uri;

    fn build_response<'r>(operation: Operation) -> (Status, OperationResult) {
        let client = Client::tracked(launch()).expect("valid rocket instance");
        let req = client.post(uri!(super::index)).json(&operation);
        let response = req.dispatch();
        (
            response.status(),
            response.into_json::<OperationResult>().unwrap(),
        )
    }

    #[test]
    fn index() {
        let client = Client::tracked(launch()).expect("valid rocket instance");
        let mut req = client.get(uri!(index));
        req.set_host(Host::from(uri!("HOST_URI")));
        let response = req.dispatch();
        assert_eq!(response.status(), Status::Ok);
    }

    #[test]
    fn plus() {
        let (status, response) = build_response(Operation::Plus(3, 8));
        assert_eq!(status, Status::Ok);

        let expected = OperationResult::new("3 + 8", 11);
        assert_eq!(response, expected);
    }

    #[test]
    fn moins() {
        let (status, response) = build_response(Operation::Moins(10, 20));
        assert_eq!(status, Status::Ok);

        let expected = OperationResult::new("10 - 20", -10);
        assert_eq!(response, expected);
    }

    #[test]
    fn fois() {
        let (status, response) = build_response(Operation::Fois(10, 20));
        assert_eq!(status, Status::Ok);

        let expected = OperationResult::new("10 x 20", 200);
        assert_eq!(response, expected);
    }

    #[test]
    fn diviser() {
        let (status, response) = build_response(Operation::Diviser(10, 20));
        assert_eq!(status, Status::Ok);

        let expected = OperationResult::new("10 / 20", 0);
        assert_eq!(response, expected);
    }
}
