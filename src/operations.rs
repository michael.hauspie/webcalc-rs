//! This modules defines the possible operation and provide methods
//! to do the actual computation

use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Copy, Clone, Debug)]
pub enum Operation {
    Plus(i32, i32),
    Moins(i32, i32),
    Fois(i32, i32),
    Diviser(i32, i32),
}

#[derive(Deserialize, Serialize, PartialEq, Debug)]
pub struct OperationResult {
    operation: String,
    resultat: i32,
}

impl From<&Operation> for OperationResult {
    fn from(value: &Operation) -> Self {
        match value {
            Operation::Plus(op1, op2) => OperationResult {
                operation: format!("{op1} + {op2}"),
                resultat: op1 + op2,
            },
            Operation::Moins(op1, op2) => OperationResult {
                operation: format!("{op1} - {op2}"),
                resultat: op1 - op2,
            },
            Operation::Fois(op1, op2) => OperationResult {
                operation: format!("{op1} x {op2}"),
                resultat: op1 * op2,
            },
            Operation::Diviser(op1, op2) => OperationResult {
                operation: format!("{op1} / {op2}"),
                resultat: op1 / op2,
            },
        }
    }
}

impl From<Operation> for OperationResult {
    fn from(value: Operation) -> Self {
        OperationResult::from(&value)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    impl OperationResult {
        /// Creates a new operation result from a [`&str`] and a value.
        pub fn new(s: &str, value: i32) -> Self {
            Self {
                operation: String::from(s),
                resultat: value,
            }
        }
    }

    #[test]
    fn plus() {
        let operation = Operation::Plus(4, 5);
        let result = OperationResult::from(operation);
        let expected = OperationResult::new("4 + 5", 9);

        assert_eq!(result, expected);
    }

    #[test]
    fn moins() {
        let operation = Operation::Moins(4, 5);
        let result = OperationResult::from(operation);
        let expected = OperationResult::new("4 - 5", -1);

        assert_eq!(result, expected);
    }

    #[test]
    fn fois() {
        let operation = Operation::Fois(4, 5);
        let result = OperationResult::from(operation);
        let expected = OperationResult::new("4 x 5", 20);

        assert_eq!(result, expected);
    }

    #[test]
    fn diviser() {
        let operation = Operation::Diviser(20, 4);
        let result = OperationResult::from(operation);
        let expected = OperationResult::new("20 / 4", 5);

        assert_eq!(result, expected);
    }
}
